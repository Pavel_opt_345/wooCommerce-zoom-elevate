<?php
/*
Plugin Name: Woocommerce ZoomElevate
Plugin URI: 
Description: 
Version: 1.0.0
Author: 
Author URI: 
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

if ( ! defined ( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

if ( ! function_exists ( 'is_plugin_active' ) ) {
    require_once ( ABSPATH . 'wp-admin/includes/plugin.php' );
}
/**
 * Check if WooCommerce is active
 **/
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
    /**
     * Localisation
     **/
    
    load_plugin_textdomain( 'woocommerce-zoom-elevate', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
    
    if ( ! class_exists( 'WC_Settings_Tab_Zoom' ) ) {
        
        class WC_Settings_Tab_Zoom {
            
            public function __construct() {
                
                add_image_size('shop_magnifier', 600, 600, true );
                //add_action( 'init', array($this, 'image_sizes_zoom'));
                
                add_filter('woocommerce_settings_tabs_array',  array($this, 'add_settings_tab'), 50);
                add_action('woocommerce_settings_tabs_settings_tab_zoom', array($this, 'settings_tab_zoom'));
                add_action( 'woocommerce_update_options_settings_tab_zoom', array($this, 'update_settings_zoom'));
                add_action('woocommerce_admin_field_checkbox_zoom', array($this, 'admin_fields_checkbox_zoom'),10, 1);
                add_action('woocommerce_admin_field_image_width_zoom', array($this, 'admin_fields_image_width_zoom'));
                
                if ( is_admin() ) {
                    add_action('init', array($this, 'zoom_enqueue_script_admin'));
                } else {
                    if(get_option('wc_settings_tab_zoomEnabled')){
                        add_action('wp_enqueue_scripts',array($this, 'zoom_enqueue_script_frontend'));
                        add_action( 'template_redirect', array($this, 'zoom_remove_add_action'));
                        //add_action( 'init', array($this, 'zoom_remove_add_action'));
                        
                        /* Turning off srcset and sizes for images in WordPress */
                        add_action( 'init', array($this, 'remove_srcset_sizes'));
                    }
                }
            }
            
            public function zoom_remove_add_action() {
                remove_action ( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20);
                remove_action ( 'woocommerce_product_thumbnails', 'woocommerce_show_product_thumbnails', 20 );
                add_action ( 'woocommerce_before_single_product_summary', array($this, 'show_product_images_zoom'), 20 );
                add_action ( 'woocommerce_product_thumbnails', array($this, 'show_product_thumbnails_zoom'), 20 );
            }
            
            public function remove_srcset_sizes() {
                add_filter('wp_calculate_image_srcset_meta', '__return_null' );
                add_filter('wp_calculate_image_sizes', '__return_false',  99 );
                remove_filter('the_content', 'wp_make_content_images_responsive' );
                add_filter('wp_get_attachment_image_attributes', 'unset_attach_srcset_attr', 99 );
                function unset_attach_srcset_attr( $attr ){
                        foreach( array('sizes','srcset') as $key )
                                if( isset($attr[ $key ]) )    unset($attr[ $key ]);
                        return $attr;
                }
            }
            
            public function zoom_enqueue_script_admin() {
                wp_enqueue_script( 'custom_Zoom', plugins_url('/assets/js/customZoom.js',__FILE__), array('jquery'), null, true);
            }
    
            public function zoom_enqueue_script_frontend() {
                wp_enqueue_script( 'jquery_easing', plugins_url('/assets/js/jquery.easing.1.3.js', __FILE__), array('jquery'), null, true);
                wp_enqueue_script( 'jquery_elevatezoom', plugins_url('/assets/js/jquery.elevateZoom.min.js', __FILE__), array('jquery'), null, true);
            }
            
            public function add_settings_tab( $settings_tabs ) {
                $settings_tabs['settings_tab_zoom'] = __( 'Zoom', '' );
                return $settings_tabs;
            }

            public function settings_tab_zoom() {
                woocommerce_admin_fields( $this->get_settings_zoom() );
            }
            
            public function update_settings_zoom() {
                woocommerce_update_options($this->get_settings_zoom());
                $this->update_settings_custom_type($this->get_settings_zoom());
            }
            
            public function update_settings_custom_type($settings) {
                
                if ( empty( $_POST ) ) {
			return false;
		}
                foreach ( $settings as $val ) {
                    if($val['type'] == 'checkbox_zoom') {
                        $checkbox = isset($_POST[$val['id']]) ? 1 : 0 ;
                        update_option( $val['id'], $checkbox );
                    }
                }
            }
            
            public function get_settings_zoom() {
                
                $settings = array(
                    'ez_section_title' => array(
                        'title'     => __( 'Elevate Zoom', '' ),
                        'type'      => 'title',
                    ),
                    'ez_zoomEnabled' => array(
                        'title'     => __( 'Zoom Enabled', '' ),
                        'desc'      => __( '', '' ),
                        'desc_tip'  => __('Default : "True',''),
                        'type'      => 'checkbox_zoom',
                        'default'   => get_option('wc_settings_tab_zoomEnabled') ? get_option('wc_settings_tab_zoomEnabled') : true,
                        'id'        => 'wc_settings_tab_zoomEnabled',
                    ),
                    'ez_responsive' => array(
                        'title'     => __( 'Responsive', '' ),
                        'desc'      => __( 'Set to true to activate responsivenes. If you have a theme which changes size, or tablets which change orientation this is needed to be on. Possible Values: "True", "False"', '' ),
                        'desc_tip'  => __('Default : "False".',''),
                        'type'      => 'checkbox_zoom',
                        'default'   => get_option('wc_settings_tab_zoom_responsive') ? get_option('wc_settings_tab_zoom_responsive') : false,
                        'id'        => 'wc_settings_tab_zoom_responsive',
                    ),
                    'ez_scrollZoom' => array(
                        'title'     => __( 'Scroll Zoom', '' ),
                        'desc'      => __('Set to true to activate zoom on mouse scroll.', ''),
                        'desc_tip'  => __('Default : "False".',''),
                        'type'      => 'checkbox_zoom',
                        'default'   => get_option('wc_settings_tab_zoom_scrollZoom') ? get_option('wc_settings_tab_zoom_scrollZoom') : false,
                        'id'        => 'wc_settings_tab_zoom_scrollZoom',
                    ),
                    'ez_cursor' => array (
                        'title'     => __( 'Cursor', '' ),
                        'desc'      => __('The default cursor is usually the arrow, if using a lightbox, then set the cursor to pointer so it looks clickable. Options are default, crosshair, move, pointer.', ''),
                        'desc_tip'  => __('Default : "Default"',''),
                        'type'      => 'select',
                        'class'    => 'wc-enhanced-select',
                        'css'      => 'min-width: 350px;',
                        'options'   => array(
                                'default'   => __( 'default', '' ),
                                'crosshair' => __( 'crosshair', '' ),
                                'move'      => __( 'move', '' ),
                                'pointer'   => __( 'pointer', '' ),
                        ),
                        'default'   => get_option('wc_settings_tab_zoom_cursor') ? get_option('wc_settings_tab_zoom_cursor') : 'default',
                        'id'        => 'wc_settings_tab_zoom_cursor'
                    ),
                    'ez_imageSize' => $this->get_image_size_zoom(),
                    'ez_zoomType' => array(
                        'title'     => __( 'Zoom Type', '' ),
                        'desc'      => __('Possible Values: Lens, Window, Inner', ''),
                        'desc_tip'  => __('Default : "Window".',''),
                        'type'      => 'select',
                        'class'    => 'wc-enhanced-select',
                        'css'      => 'min-width: 350px;',
                        'options'   => array(
                                'lens'      => __( 'Lens', '' ),
                                'window' => __( 'Window', '' ),
                                'inner' => __( 'Inner', '' )
                        ),
                        'default'   => get_option('wc_settings_tab_zoom_zoomType') ? get_option('wc_settings_tab_zoom_zoomType') : 'window',
                        'id'        => 'wc_settings_tab_zoom_zoomType',
                    ),
                    
                    /*===== Window =====*/
                    /*  Zoom Size Window  */
                    'ez_zoomLevel' => array(
                        'title'     => __( 'Zoom Level', '' ),
                        'desc'      => __('', ''),
                        'desc_tip'  =>__('Default : "1"', ''),
                        'type'      => 'text',
                        'class'    => 'window-select',
                        'css'     => 'min-width: 350px; display: block;',
                        'placeholder'=> __( '1', '' ),
                        'default'   => get_option('wc_settings_tab_zoomLevel') ? get_option('wc_settings_tab_zoomLevel') : '1',
                        'id'        => 'wc_settings_tab_zoomLevel'
                    ),
                    'ez_zoomWindowWidth' => array(
                        'title'     => __( 'Zoom Window Width', '' ),
                        'desc'      => __('Width of the zoomWindow', ''),
                        'desc_tip'  =>__('Note: zoomType must be "Window"', ''),
                        'type'      => 'text',
                        'class'    => 'window-select',
                        'css'     => 'min-width: 350px; display: block;',
                        'placeholder'=> __( '400', '' ),
                        'default'   => get_option('wc_settings_tab_zoomWindowWidth') ? get_option('wc_settings_tab_zoomWindowWidth') : '400',
                        'id'        => 'wc_settings_tab_zoomWindowWidth'
                    ),
                    'ez_zoomWindowHeight' => array(
                        'title'     => __( 'Zoom Window Height', '' ),
                        'desc'      => __('Height of the zoomHeight', ''),
                        'desc_tip'  =>__('Note: zoomType must be "Window"', ''),
                        'type'      => 'text',
                        'class'    => 'window-select',
                        'css'     => 'min-width: 350px; display: block;',
                        'placeholder'=> __( '400', '' ),
                        'default'   => get_option('wc_settings_tab_zoomWindowHeight') ? get_option('wc_settings_tab_zoomWindowHeight') : '400',
                        'id'        => 'wc_settings_tab_zoomWindowHeight'
                    ),
                    /*  Zoom Offet Window  */
                    'ez_zoomWindowOffetx' => array(
                        'title'     => __( 'Zoom Window Offet X', '' ),
                        'desc'      => __('x-axis offset of the zoom window', ''),
                        'desc_tip'  =>__('Default : "0"', ''),
                        'type'      => 'text',
                        'class'    => 'window-select',
                        'css'     => 'min-width: 350px; display: block;',
                        'placeholder'=> __( '0', '' ),
                        'default'   => get_option('wc_settings_tab_zoomWindowOffetx') ? get_option('wc_settings_tab_zoomWindowOffetx') : '0',
                        'id'        => 'wc_settings_tab_zoomWindowOffetx'
                    ),
                    'ez_zoomWindowOffety' => array(
                        'title'     => __( 'Zoom Window Offet Y', '' ),
                        'desc'      => __('y-axis offset of the zoom window', ''),
                        'desc_tip'  =>__('Default : "0"', ''),
                        'type'      => 'text',
                        'class'    => 'window-select',
                        'css'     => 'min-width: 350px; display: block;',
                        'placeholder'=> __( '0', '' ),
                        'default'   => get_option('wc_settings_tab_zoomWindowOffety') ? get_option('wc_settings_tab_zoomWindowOffety') : '0',
                        'id'        => 'wc_settings_tab_zoomWindowOffety'
                    ),
                    /*  Zoom Position Window  */
                    'ez_zoomWindowPosition' => array(
                        'title'     => __( 'Zoom Window Position', '' ),
                        'desc'      => __('Once positioned, use zoomWindowOffsetx and zoomWindowOffsety to adjust Possible values: 1-16', ''),
                        'desc_tip'  =>__('Default : "1"', ''),
                        'type'      => 'number',
                        'class'    => 'window-select',
                        'css'     => 'min-width: 80px; width: 80px; display: block;',
                        'placeholder'=> __( '1', '' ),
                        'custom_attributes' => array(
                                'min'   => 1,
                                'max'   => 16,
				'step' => 1
                        ),
                        'default'   => get_option('wc_settings_tab_zoomWindowPosition') ? get_option('wc_settings_tab_zoomWindowPosition') : '1',
                        'id'        => 'wc_settings_tab_zoomWindowPosition'
                    ),
                    /*  Setting Tint  */
                    'ez_tint' => array(
                        'title'     => __( 'Tint', '' ),
                        'desc'      => __('Enable a tint overlay.', ''),
                        'desc_tip'  =>__('Default : "False"', ''),
                        'type'      => 'checkbox_zoom',
                        'class'    => 'window-select',
                        'default'   => get_option('wc_settings_tab_zoom_tint') ? get_option('wc_settings_tab_zoom_tint') : false,
                        'id'        => 'wc_settings_tab_zoom_tint',
                    ),
                    'ez_tintColour' => array(
                        'title'    => __( 'Tint Colour', '' ),
                        'desc'     => __( 'Colour of the tint, can be #hex, word (red, blue), or rgb(x, x, x).', '' ),
                        'desc_tip' => __( 'Default <code>#333333</code>.', '' ),
                        'type'     => 'color',
                        'class'    => 'window_tint_select ',
                        'css'     => 'min-width: 324px;',
                        'placeholder'=> __( '#333333', '' ),
                        'default'  => get_option('wc_settings_tab_zoom_tintColour') ? get_option('wc_settings_tab_zoom_tintColour') : '#333333',
                        'id'       => 'wc_settings_tab_zoom_tintColour'
                    ),
                    'ez_tintOpacity' => array(
                        'title'     => __( 'Tint Opacity', '' ),
                        'desc'      => __('Opacity of the tint', ''),
                        'desc_tip'  =>__('Default : "0.4"', ''),
                        'type'      => 'number',
                        'class'     => 'window_tint_select',
                        'css'       => 'min-width: 80px; width: 80px; display: block;',
                        'placeholder'=> __( '0.4', '' ),
                        'custom_attributes' => array(
                                'min'   => 0,
                                'max'   => 1,
                                'step' => 0.1
                        ),
                        'default'   => get_option('wc_settings_tab_zoom_tintOpacity') ? get_option('wc_settings_tab_zoom_tintOpacity') : '0.4',
                        'id'        => 'wc_settings_tab_zoom_tintOpacity'
                    ),
                    /*  Zoom Style Window  */
                    'ez_lensBorderSize' => array(
                        'title'     => __( 'Lens Border', '' ),
                        'desc'      => __('Width in pixels of the lens border', ''),
                        'desc_tip'  =>__('Default : "1"', ''),
                        'type'      => 'text',
                        'class'    => 'window-select',
                        'css'     => 'min-width: 350px; display: block;',
                        'placeholder'=> __( '1', '' ),
                        'default'   => get_option('wc_settings_tab_zoom_lensBorderSize') ? get_option('wc_settings_tab_zoom_lensBorderSize') : '1',
                        'id'        => 'wc_settings_tab_zoom_lensBorderSize'
                    ),
                    'ez_lensBorderColour' => array(
                        'title'     => __( 'Lens Border Colour', '' ),
                        'desc'      => __('Colour of the lens border', ''),
                        'desc_tip'  =>__('Default : "#000000"', ''),
                        'type'      => 'color',
                        'class'    => 'window-select ',
                        'css'     => 'min-width: 324px;',
                        'placeholder'=> __( '#000000', '' ),
                        'default'   => get_option('wc_settings_tab_zoom_lensBorderColour') ? get_option('wc_settings_tab_zoom_lensBorderColour') : '#000000',
                        'id'        => 'wc_settings_tab_zoom_lensBorderColour'
                    ),
                    'ez_lensColour' => array(
                        'title'     => __( 'Lens Colour', '' ),
                        'desc'      => __('Colour of the lens background', ''),
                        'desc_tip'  =>__('Default : "#FFFFFF"', ''),
                        'type'      => 'color',
                        'class'    => 'window-select ',
                        'css'     => 'min-width: 324px;',
                        'placeholder'=> __( '#FFFFFF', '' ),
                        'default'   => get_option('wc_settings_tab_zoom_lensColour') ? get_option('wc_settings_tab_zoom_lensColour') : '#FFFFFF',
                        'id'        => 'wc_settings_tab_zoom_lensColour'
                    ),
                    'ez_lensOpacity' => array(
                        'title'     => __( 'Lens Opacity', '' ),
                        'desc'      => __('used in combination with lensColour to make the lens see through. When using tint, this is overrided to 0', ''),
                        'desc_tip'  =>__('Default : "0.4"', ''),
                        'type'      => 'number',
                        'class'    => 'window-select',
                        'css'     => 'min-width: 80px; width: 80px; display: block;',
                        'placeholder'=> __( '0.4', '' ),
                        'custom_attributes' => array(
                                'min'   => 0,
                                'max'   => 1,
                                'step' => 0.1
                        ),
                        'default'   => get_option('wc_settings_tab_zoom_lensOpacity') ? get_option('wc_settings_tab_zoom_lensOpacity') : '0.4',
                        'id'        => 'wc_settings_tab_zoom_lensOpacity'
                    ),
                    
                    /*===== Lens Window=====*/
                    /* Setting Border */
                    'ez_borderSize' => array(
                        'title'     => __( 'Border Size', '' ),
                        'desc'      => __('Border Size of the ZoomBox - Must be set here as border taken into account for plugin calculations', ''),
                        'desc_tip'  =>__('Default : "4"', ''),
                        'type'      => 'text',
                        'class'    => 'window_lens_select',
                        'css'     => 'min-width: 350px; display: block;',
                        'placeholder'=> __( '4', '' ),
                        'default'   => get_option('wc_settings_tab_zoom_borderSize') ? get_option('wc_settings_tab_zoom_borderSize') : '4',
                        'id'        => 'wc_settings_tab_zoom_borderSize'
                    ),
                    'ez_borderColour' => array(
                        'title'     => __( 'Border Colour', '' ),
                        'desc'      => __('Border Colour', ''),
                        'desc_tip'  =>__('Default : "#888888"', ''),
                        'type'      => 'color',
                        'class'    => 'window_lens_select ',
                        'css'     => 'min-width: 324px;',
                        'placeholder'=> __( '#888888', '' ),
                        'default'   => get_option('wc_settings_tab_zoom_borderColour') ? get_option('wc_settings_tab_zoom_borderColour') : '#888888',
                        'id'        => 'wc_settings_tab_zoom_borderColour'
                    ),
                    
                    /*===== Lens =====*/
                    'ez_showLens' => array(
                        'title'     => __( 'Show Lens', '' ),
                        'desc'      => __('Set to false to deactivate Lens', ''),
                        'desc_tip'  => __('Default : "True".',''),
                        'type'      => 'checkbox_zoom',
                        'class'    => 'lens_select',
                        'default'   => get_option('wc_settings_tab_zoom_showLens') ? get_option('wc_settings_tab_zoom_showLens') : true,
                        'id'        => 'wc_settings_tab_zoom_showLens',
                    ),
                    'ez_lensSize' => array(
                        'title'     => __( 'Lens Size', '' ),
                        'desc'      => __('Used when zoomType set to lens, when zoom type is set to window, then the lens size is auto calculated', ''),
                        'desc_tip'  =>__('Default : "200"', ''),
                        'type'      => 'text',
                        'class'    => 'lens_select',
                        'css'     => 'min-width: 350px; display: block;',
                        'placeholder'=> __( '200', '' ),
                        'default'   => get_option('wc_settings_tab_zoom_lensSize') ? get_option('wc_settings_tab_zoom_lensSize') : '200',
                        'id'        => 'wc_settings_tab_zoom_lensSize'
                    ),
                    'ez_lensShape' => array(
                        'title'     => __( 'Lens Shape', '' ),
                        'desc'      => __('Can also be round (note that only modern browsers support round, will default to square in older browsers)', ''),
                        'desc_tip'  => __('Default : "Square".',''),
                        'type'      => 'select',
                        'class'    => 'wc-enhanced-select lens_select',
                        'css'     => 'min-width: 350px;',
                        'options'   => array(
                                'square'     => __( 'Square', '' ),
                                'round'      => __( 'Round', '' )
                        ),
                        'default'   => get_option('wc_settings_tab_zoom_lensShape') ? get_option('wc_settings_tab_zoom_lensShape') : 'square',
                        'id'        => 'wc_settings_tab_zoom_lensShape',
                    ),
                    'ez_containLensZoom' => array(
                        'title'     => __( 'Contain Lens Zoom', '' ),
                        'desc'      => __('for use with the Lens Zoom Type. This makes sure the lens does not fall outside the outside of the image.', ''),
                        'desc_tip'  => __('Default : "False".',''),
                        'type'      => 'checkbox_zoom',
                        'class'    => 'lens_select',
                        'default'   => get_option('wc_settings_tab_zoom_containLensZoom') ? get_option('wc_settings_tab_zoom_containLensZoom') : false,
                        'id'        => 'wc_settings_tab_zoom_containLensZoom',
                    ),
                    
                    /*===== Setting Easing =====*/
                    'ez_easing' => array(
                        'title'     => __( 'Easing', '' ),
                        'desc'      => __('Set to true to activate easing. Possible Values: "True", "False"', ''),
                        'desc_tip'  =>__('Default : "False"', ''),
                        'type'      => 'checkbox_zoom',
                        'class'    => 'window_inner_easing',
                        'default'   => get_option('wc_settings_tab_zoom_easing') ? get_option('wc_settings_tab_zoom_easing') : false,
                        'id'        => 'wc_settings_tab_zoom_easing',
                    ),
                    'ez_easingAmount' => array(
                        'title'     => __( 'Easing Duration', '' ),
                        'desc'      => __('', ''),
                        'desc_tip'  =>__('Default : "12"', ''),
                        'type'      => 'text',
                        'class'    => 'window_inner_easing',
                        'css'     => 'min-width: 350px; display: block;',
                        'placeholder'=> __( '12', '' ),
                        'default'   => get_option('wc_settings_tab_zoom_easingAmount') ? get_option('wc_settings_tab_zoom_easingAmount') : '12',
                        'id'        => 'wc_settings_tab_zoom_easingAmount'
                    ),
                    
                    /*===== FadeIn/FadeOut  =====*/
                    'ez_zoomWindowFadeIn' => array(
                        'title'     => __( 'Zoom Window FadeIn', '' ),
                        'desc'      => __('Set as a number e.g 200 for speed of Window fadeIn', ''),
                        'desc_tip'  =>__('Default : "False"', ''),
                        'type'      => 'text',
                        'class'    => 'window_inner_fade',
                        'css'     => 'min-width: 350px; display: block;',
                        'placeholder'=> __( '0', '' ),
                        'default'   => get_option('wc_settings_tab_zoomWindowFadeIn') ? get_option('wc_settings_tab_zoomWindowFadeIn') : false,
                        'id'        => 'wc_settings_tab_zoomWindowFadeIn'
                    ),
                    'ez_zoomWindowFadeOut' => array(
                        'title'     => __( 'Zoom Window FadeOut', '' ),
                        'desc'      => __('Set as a number e.g 200 for speed of Window fadeOut', ''),
                        'desc_tip'  =>__('Default : "False"', ''),
                        'type'      => 'text',
                        'class'    => 'window_inner_fade',
                        'css'     => 'min-width: 350px; display: block;',
                        'placeholder'=> __( '0', '' ),
                        'default'   => get_option('wc_settings_tab_zoomWindowFadeOut') ? get_option('wc_settings_tab_zoomWindowFadeOut') : false,
                        'id'        => 'wc_settings_tab_zoomWindowFadeOut'
                    ),
                    'ez_lensFadeIn' => array(
                        'title'     => __( 'Lens FadeIn', '' ),
                        'desc'      => __('Set as a number e.g 200 for speed of Lens fadeIn', ''),
                        'desc_tip'  =>__('Default : "False"', ''),
                        'type'      => 'text',
                        'class'    => 'window_lens_fade',
                        'css'     => 'min-width: 350px; display: block;',
                        'placeholder'=> __( '0', '' ),
                        'default'   => get_option('wc_settings_tab_zoom_lensFadeIn') ? get_option('wc_settings_tab_zoom_lensFadeIn') : false,
                        'id'        => 'wc_settings_tab_zoom_lensFadeIn'
                    ),
                    'ez_lensFadeOut' => array(
                        'title'     => __( 'Lens FadeOut', '' ),
                        'desc'      => __('Set as a number e.g 200 for speed of Lens fadeOut', ''),
                        'desc_tip'  =>__('Default : "False"', ''),
                        'type'      => 'text',
                        'class'    => 'window_lens_fade',
                        'css'     => 'min-width: 350px; display: block;',
                        'placeholder'=> __( '0', '' ),
                        'default'   => get_option('wc_settings_tab_zoom_lensFadeOut') ? get_option('wc_settings_tab_zoom_lensFadeOut') : false,
                        'id'        => 'wc_settings_tab_zoom_lensFadeOut'
                    ),
                    'ez_zoomTintFadeIn' => array(
                        'title'     => __( 'Tint FadeIn', '' ),
                        'desc'      => __('Set as a number e.g 200 for speed of Tint fadeIn', ''),
                        'desc_tip'  =>__('Default : "False"', ''),
                        'type'      => 'text',
                        'class'    => 'window_tint_fade',
                        'css'     => 'min-width: 350px; display: block;',
                        'placeholder'=> __( '0', '' ),
                        'default'   => get_option('wc_settings_tab_zoomTintFadeIn') ? get_option('wc_settings_tab_zoomTintFadeIn') : false,
                        'id'        => 'wc_settings_tab_zoomTintFadeIn'
                    ),
                    'ez_zoomTintFadeOut' => array(
                        'title'     => __( 'Tint FadeOut', '' ),
                        'desc'      => __('Set as a number e.g 200 for speed of Tint fadeOut', ''),
                        'desc_tip'  =>__('Default : "False"', ''),
                        'type'      => 'text',
                        'class'    => 'window_tint_fade',
                        'css'     => 'min-width: 350px; display: block;',
                        'placeholder'=> __( '0', '' ),
                        'default'   => get_option('wc_settings_tab_zoomTintFadeOut') ? get_option('wc_settings_tab_zoomTintFadeOut') : false,
                        'id'        => 'wc_settings_tab_zoomTintFadeOut'
                    ),
                    
                    /*===== Setting Gallery =====*/
                    'ez_gallery' => array(
                        'title'     => __( 'Gallery', '' ),
                        'desc'      => __('This assigns a set of gallery links to the zoom image', ''),
                        'desc_tip'  =>__('Default : "gallery_image"', ''),
                        'type'      => 'text',
                        'placeholder'=> __( 'gallery_image', '' ),
                        'default'   => get_option('wc_settings_tab_zoom_gallery') ? get_option('wc_settings_tab_zoom_gallery') : 'gallery_image',
                        'id'        => 'wc_settings_tab_zoom_gallery'
                    ),
                    'ez_imageCrossfade' => array(
                        'title'     => __( 'Image Crossfade', '' ),
                        'desc'      => __('Set to true to activate simultaneous crossfade of images on gallery change. Possible Values: "True", "False" когда включена не работает Tint в Window ', ''),
                        'desc_tip'  =>__('Default : "False"', ''),
                        'type'      => 'checkbox_zoom',
                        'default'   => get_option('wc_settings_tab_zoom_imageCrossfade') ? get_option('wc_settings_tab_zoom_imageCrossfade') : false,
                        'id'        => 'wc_settings_tab_zoom_imageCrossfade',
                    ),
                    'ez_responsiveSize' => array(
                        'title'     => __( 'Responsive Size', '' ),
                        'type'      => 'text',
                        'desc'      => __('Размер окна', ''),
                        'desc_tip'  =>__('Default : "600"', ''),
                        'placeholder'=> __( '600', '' ),
                        'default'   => get_option('ez_responsiveSize') ? get_option('ez_responsiveSize') : '600',
                        'id'        => 'wc_settings_tab_zoom_responsiveSize'
                    ),
                    
                    
                    'ez_section_end' => array(
                         'type' => 'sectionend',
                         'id' => 'wc_settings_tab_zoom_section_end'
                    )
                );
                return apply_filters( 'wc_settings_tab_zoom_settings', $settings );
            }
            
            public function get_image_size_zoom() {        
                $image_size = array(
                    'title'     => __( 'Image Size', '' ),
                    'desc_tip'  => __( 'The size of the images used within the magnifier box', '' ),
                    'type'      => 'image_width_zoom',
                    'placeholder' => array(
                        'width'  => 600,
                        'height' => 600,
                    ),
                    'default'  => array(
                        'width'  => 600,
                        'height' => 600,
                        'crop'   => true,
                    ),
                    'id'       => 'wc_settings_tab_zoom_magnifier_image',
                );
                return $image_size;
            }
            
            public static function show_settings_general() {
                
                $settings = array();
                
                if(get_option('wc_settings_tab_zoom_responsive') == true){
                    $settings['responsive'] = true;
                }
                if(get_option('wc_settings_tab_zoom_zoomType') != 'inner'){
                    if(get_option('wc_settings_tab_zoom_scrollZoom') == true){
                        $settings['scrollZoom'] = true;
                    }
                }
                if(get_option('wc_settings_tab_zoom_cursor')){
                    $settings['cursor'] = get_option('wc_settings_tab_zoom_cursor');
                }
                if(get_option('wc_settings_tab_zoom_zoomType')){
                    $settings['zoomType'] = get_option('wc_settings_tab_zoom_zoomType');
                }
                /*===== Gallery =====*/
                if(get_option('wc_settings_tab_zoom_gallery')){
                    $settings['gallery'] = get_option('wc_settings_tab_zoom_gallery');
                }
                if(get_option('wc_settings_tab_zoom_imageCrossfade') == true){
                    $settings['imageCrossfade'] = true;
                }
                $settings['galleryActiveClass'] = 'active';
                
                return $settings;
            }
            
            public static function show_settings_window() {
                
                $settings = array();
                
                if(get_option('wc_settings_tab_zoom_zoomType') == 'window'){
                    if(get_option('wc_settings_tab_zoomLevel')){
                        $settings['zoomLevel'] = floatval(str_replace(',','.',get_option('wc_settings_tab_zoomLevel')));
                    }
                    if(get_option('wc_settings_tab_zoomWindowWidth')){
                        $settings['zoomWindowWidth'] = intval(get_option('wc_settings_tab_zoomWindowWidth'));
                    }
                    if(get_option('wc_settings_tab_zoomWindowHeight')){
                        $settings['zoomWindowHeight'] = intval(get_option('wc_settings_tab_zoomWindowHeight'));
                    }
                    if(get_option('wc_settings_tab_zoomWindowOffetx')){
                        $settings['zoomWindowOffetx'] = intval(get_option('wc_settings_tab_zoomWindowOffetx'));
                    }
                    if(get_option('wc_settings_tab_zoomWindowOffety')){
                        $settings['zoomWindowOffety'] = intval(get_option('wc_settings_tab_zoomWindowOffety'));
                    }
                    if(get_option('wc_settings_tab_zoomWindowPosition')){
                        $settings['zoomWindowPosition'] = intval(get_option('wc_settings_tab_zoomWindowPosition'));
                    }
                    if(get_option('wc_settings_tab_zoom_tint') == true){
                        $settings['tint'] = true;
                        if(get_option('wc_settings_tab_zoom_tintColour')){
                            $settings['tintColour'] = get_option('wc_settings_tab_zoom_tintColour');
                        }
                        if(get_option('wc_settings_tab_zoom_tintOpacity')){
                            $settings['tintOpacity'] = floatval(str_replace(',','.',get_option('wc_settings_tab_zoom_tintOpacity')));
                        }
                        if(get_option('wc_settings_tab_zoomTintFadeIn')){
                        $settings['zoomTintFadeIn'] = intval(get_option('wc_settings_tab_zoomTintFadeIn'));
                        }
                        if(get_option('wc_settings_tab_zoomTintFadeOut')){
                            $settings['zoomTintFadeOut'] = intval(get_option('wc_settings_tab_zoomTintFadeOut'));
                        }
                    }
                    if(get_option('wc_settings_tab_zoom_lensBorderSize')){
                        $settings['lensBorderSize'] = intval(get_option('wc_settings_tab_zoom_lensBorderSize'));
                    }
                    if(get_option('wc_settings_tab_zoom_lensBorderColour')){
                        $settings['lensBorderColour'] = get_option('wc_settings_tab_zoom_lensBorderColour');
                    }
                    if(get_option('wc_settings_tab_zoom_tint') == false){
                        if(get_option('wc_settings_tab_zoom_lensColour')){
                            $settings['lensColour'] = get_option('wc_settings_tab_zoom_lensColour'); //lens window при tint":true -не работает
                        }
                        if(get_option('wc_settings_tab_zoom_lensOpacity')){
                            $settings['lensOpacity'] = floatval(str_replace(',','.',get_option('wc_settings_tab_zoom_lensOpacity'))); //lens window при tint":true -не работает
                        }
                    }
                    if(get_option('wc_settings_tab_zoom_borderSize')){
                        $settings['borderSize'] = intval(get_option('wc_settings_tab_zoom_borderSize'));
                    }
                    if(get_option('wc_settings_tab_zoom_borderColour')){
                        $settings['borderColour'] = get_option('wc_settings_tab_zoom_borderColour');
                    }
                    if(get_option('wc_settings_tab_zoom_easing') == true){
                        $settings['easing'] = true;
                        if(get_option('wc_settings_tab_zoom_easingAmount')){
                            $settings['easingAmount'] = intval(get_option('wc_settings_tab_zoom_easingAmount'));
                        }
                    }
                    if(get_option('wc_settings_tab_zoomWindowFadeIn')){
                        $settings['zoomWindowFadeIn'] = intval(get_option('wc_settings_tab_zoomWindowFadeIn'));
                    }
                    if(get_option('wc_settings_tab_zoomWindowFadeOut')){
                        $settings['zoomWindowFadeOut'] = intval(get_option('wc_settings_tab_zoomWindowFadeOut'));
                    }
                    if(get_option('wc_settings_tab_zoom_lensFadeIn')){
                        $settings['lensFadeIn'] = intval(get_option('wc_settings_tab_zoom_lensFadeIn'));
                    }
                    if(get_option('wc_settings_tab_zoom_lensFadeOut')){
                        $settings['lensFadeOut'] = intval(get_option('wc_settings_tab_zoom_lensFadeOut'));
                    }
                }
                return $settings;
            }
            
            public static function show_settings_lens() {
                
                $settings = array();
                
                if(get_option('wc_settings_tab_zoom_zoomType') == 'lens'){
                    
                    if(get_option('wc_settings_tab_zoom_borderSize')){
                        $settings['borderSize'] = intval(get_option('wc_settings_tab_zoom_borderSize'));
                    }
                    if(get_option('wc_settings_tab_zoom_borderColour')){
                        $settings['borderColour'] = get_option('wc_settings_tab_zoom_borderColour');
                    }
                    if(get_option('wc_settings_tab_zoom_showLens') == true){
                        $settings['showLens'] = true;
                    }
                    if(get_option('wc_settings_tab_zoom_lensSize')){
                        $settings['lensSize'] = intval(get_option('wc_settings_tab_zoom_lensSize'));
                    }
                    if(get_option('wc_settings_tab_zoom_lensShape')){
                        $settings['lensShape'] = get_option('wc_settings_tab_zoom_lensShape');
                    }
                    if(get_option('wc_settings_tab_zoom_containLensZoom') == true){
                        $settings['containLensZoom'] = true;
                    }
                    if(get_option('wc_settings_tab_zoom_lensFadeIn')){
                        $settings['lensFadeIn'] = intval(get_option('wc_settings_tab_zoom_lensFadeIn'));
                    }
                    if(get_option('wc_settings_tab_zoom_lensFadeOut')){
                        $settings['lensFadeOut'] = intval(get_option('wc_settings_tab_zoom_lensFadeOut'));
                    }
                }
                return $settings;
            }
            
            public static function show_settings_inner() {
                
                $settings = array();
                
                if(get_option('wc_settings_tab_zoom_zoomType') == 'inner'){
                    
                    if(get_option('wc_settings_tab_zoom_easing') == true){
                        $settings['easing'] = true;
                        if(get_option('wc_settings_tab_zoom_easingAmount')){
                            $settings['easingAmount'] = intval(get_option('wc_settings_tab_zoom_easingAmount'));
                        }
                    }
                    if(get_option('wc_settings_tab_zoomWindowFadeIn')){
                        $settings['zoomWindowFadeIn'] = intval(get_option('wc_settings_tab_zoomWindowFadeIn'));
                    }
                    if(get_option('wc_settings_tab_zoomWindowFadeOut')){
                        $settings['zoomWindowFadeOut'] = intval(get_option('wc_settings_tab_zoomWindowFadeOut'));
                    }
                }
                return $settings;
            }
            
            public static function show_product_script_zoom(){
                
                $settings_general = WC_Settings_Tab_Zoom::show_settings_general();
                $settings_window = WC_Settings_Tab_Zoom::show_settings_window();
                $settings_lens = WC_Settings_Tab_Zoom::show_settings_lens();
                $settings_inner = WC_Settings_Tab_Zoom::show_settings_inner();
                
                $settings = array_merge($settings_general, $settings_window, $settings_lens, $settings_inner);
                
                return json_encode($settings);
            }

            public static function show_product_script_responsive(){

                $settings_size = array();

                if(get_option('wc_settings_tab_zoom_responsive') == 'yes'){
                    $settings_size['responsive'] = true;
                }
                $settings_size['zoomType'] = 'inner';
                $settings_size['cursor'] = 'crosshair';
                /*===== FadeIn/FadeOut  24=====*/
                if(get_option('wc_settings_tab_zoom_easing') == true){
                    $settings['easing'] = true;
                    if(get_option('wc_settings_tab_zoom_easingAmount')){
                        $settings['easingAmount'] = intval(get_option('wc_settings_tab_zoom_easingAmount'));
                    }
                }
                if(get_option('wc_settings_tab_zoomWindowFadeIn')){
                    $settings['zoomWindowFadeIn'] = intval(get_option('wc_settings_tab_zoomWindowFadeIn'));
                }
                if(get_option('wc_settings_tab_zoomWindowFadeOut')){
                    $settings['zoomWindowFadeOut'] = intval(get_option('wc_settings_tab_zoomWindowFadeOut'));
                }
                /*===== Gallery =====*/
                if(get_option('wc_settings_tab_zoom_gallery')){
                    $settings_size['gallery'] = get_option('wc_settings_tab_zoom_gallery');
                }
                if(get_option('wc_settings_tab_zoom_imageCrossfade') == 'yes'){
                    $settings_size['imageCrossfade'] = true;
                }
                $settings_size['galleryActiveClass'] = 'active';

                return json_encode($settings_size);
            }
            
            public function admin_fields_checkbox_zoom( $settings ) {
                
                $checked = WC_Admin_Settings::get_option($settings['id']) ? true : false;
                
                if ( ! isset( $settings['type'] ) ) {
                    continue;
                }
                if ( ! isset( $settings['title'] ) ) {
                    $value['title'] = '';
                }
                if ( ! isset( $settings['desc'] ) ) {
                    $value['desc'] = '';
                }
                if ( ! isset( $settings['desc_tip'] ) ) {
                    $value['desc_tip'] = '';
                }
                if ( ! isset( $settings['placeholder'] ) ) {
                    $settings['placeholder'] = '';
                }
                if ( ! isset( $settings['default'] ) ) {
                    $settings['default'] = '';
                }
                if ( ! isset( $settings['id'] ) ) {
                    $settings['id'] = '';
                }
                if ( ! isset( $settings['class'] ) ) {
                    $settings['class'] = '';
                }
                if ( ! isset( $settings['css'] ) ) {
                    $settings['css'] = '';
                }

                switch ( $settings['type'] ) {
                    case 'checkbox_zoom':
                        ?>
                        <tr class="" valign="top">
                            <th class="titledesc" scope="row">
                                <label for="<?php echo esc_attr($settings['id']); ?>"><?php echo esc_html($settings['title']) ?></label>
                                <?php echo wc_help_tip( $settings['desc_tip'] ); ?>
                            </th>
                            <td class="forminp forminp-checkbox">
                                <fieldset>
                                    <legend class="screen-reader-text">
                                        <span><?php echo esc_html($settings['title']) ?></span>
                                    </legend>
                                    <label for="<?php echo esc_attr($settings['id']); ?>">
                                        <input id="<?php echo esc_attr($settings['id']);?>"
                                               class="<?php echo esc_attr($settings['class']);?>"
                                               name="<?php echo esc_attr($settings['id']); ?>"
                                               value="1"
                                               type="checkbox"
                                               style="<?php echo esc_attr($settings['css']);?>"
                                               <?php checked(1, $checked); ?> 
                                       />
                                            <span class="description"><?php echo esc_html($settings['desc']) ?></span>
                                    </label>
                                </fieldset>
                            </td>
                        </tr>
                        <?php
                        break;
                    default:
                        break;
                }
            }
            
            public function admin_fields_image_width_zoom($image_size) {
                
                $width = WC_Admin_Settings::get_option($image_size['id'].'[width]', $image_size['default']['width']);
                $height = WC_Admin_Settings::get_option($image_size['id'].'[height]', $image_size['default']['height']);
                $crop = WC_Admin_Settings::get_option($image_size['id'] .'[crop]') ? true : false;
                //$crop = checked(1, $crop);

                ?>
                <tr valign="top">
                    <th class="titledesc" scope="row">
                        <label for="<?php echo esc_attr($image_size['id']); ?>"><?php echo esc_html($image_size['title']) ?></label>
                        <?php echo wc_help_tip( $image_size['desc_tip'] ); ?>
                    </th>
                    <td class="forminp image_width_settings">
                        <input name="<?php echo esc_attr($image_size['id']); ?>[width]"
                               id="<?php echo esc_attr($image_size['id']); ?>-width" 
                               type="text" 
                               size="3"
                               value="<?php echo $width; ?>"
                               placeholder="<?php echo esc_attr($image_size['placeholder']['width']); ?>"/>
                        &times; 
                        <input name="<?php echo esc_attr($image_size['id']); ?>[height]"
                               id="<?php echo esc_attr($image_size['id']); ?>-height" 
                               type="text" 
                               size="3"
                               value="<?php echo $height; ?>"
                               placeholder="<?php echo esc_attr($image_size['placeholder']['height']); ?>"/>px 

                        <label>
                            <input name="<?php echo esc_attr($image_size['id']); ?>[crop]"
                                   id="<?php echo esc_attr($image_size['id']); ?>-crop"
                                   type="checkbox" 
                                   <?php checked(1, $crop); ?> /> <?php _e('Hard Crop?', 'woocommerce'); ?>
                        </label>
                    </td>
                </tr>
                <?php
            }
            
            public function show_product_images_zoom() {
                /** FIX WOO 2.1 */
                $wc_get_template = function_exists ( 'wc_get_template' ) ? 'wc_get_template' : 'woocommerce_get_template';
                $wc_get_template ( 'single-product/product-image-zoom.php', array (), '', plugin_dir_path ( __FILE__ ) . 'templates/' );
            }

            public function show_product_thumbnails_zoom() {
                /** FIX WOO 2.1 */
                $wc_get_template = function_exists ( 'wc_get_template' ) ? 'wc_get_template' : 'woocommerce_get_template';
                $wc_get_template( 'single-product/product-thumbnails-zoom.php', array (), '', plugin_dir_path( __FILE__ ) . 'templates/' );
            }
            
            public function image_sizes_zoom() {
                
                $image_size = $this->get_image_size_zoom();
                var_dump($image_size);
                $width = WC_Admin_Settings::get_option($image_size['id'].'[width]') ? WC_Admin_Settings::get_option($image_size['id'].'[width]') : $image_size['default']['width'];
                $height = WC_Admin_Settings::get_option($image_size['id'].'[height]') ? WC_Admin_Settings::get_option($image_size['id'].'[height]') : $image_size['default']['height'];
                $crop   = WC_Admin_Settings::get_option($image_size['id'].'[crop]') ? true : false;

                $ez_imageSize = array(
                    'width' => $width,
                    'height' => $height,
                    'crop' => $crop
                );
                update_option('shop_magnifier', $ez_imageSize );
//                add_image_size( 'shop_magnifier', $width, $height, $crop );
            }
        }
        $WC_Settings_Tab_Zoom = new WC_Settings_Tab_Zoom();
        
    }
    
}