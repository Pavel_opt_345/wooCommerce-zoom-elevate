<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $woocommerce, $product;

?>
<div class="images">
	<?php
		if ( has_post_thumbnail() ) {
                        $image_ar = image_downsize(get_post_thumbnail_id(), 'shop_magnifier');
                        
                        if($image_ar['3'] == false) {
                           WC_Settings_Tab_Zoom::image_editor_zoom(get_post_thumbnail_id());
                        }
			
                        $image_link = wp_get_attachment_image_src(get_post_thumbnail_id(), 'shop_magnifier');
                        
			$image         = get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
                                'id' => 'zoom_image',
                                'class' => 'elevate-zoom',
                                'title'	=> get_the_title( get_post_thumbnail_id()),
                                'data-zoom-image'  =>  $image_link[0]
                                )
			);
                        
			echo apply_filters( 'woocommerce_single_product_image_html', $image, $post->ID );

		} else {

			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), __( 'Placeholder', 'woocommerce' ) ), $post->ID );

		}
	?>

	<?php do_action( 'woocommerce_product_thumbnails' ); ?>
</div>
<script type="text/javascript" charset="utf-8">
    
    var doc_w='';
    var responsiveSize = <?php echo intval(get_option('wc_settings_tab_zoom_responsiveSize')); ?>;
    var settings = <? echo WC_Settings_Tab_Zoom::show_product_script_zoom(); ?>;
    var settings_size = <? echo WC_Settings_Tab_Zoom::show_product_script_responsive(); ?>;
    
    var _change_doc_w = function(doc_w){        
        if(doc_w > responsiveSize){
            if(jQuery("div").is(".zoomContainer")){
                jQuery("div.zoomContainer").remove();
            }
            jQuery('#zoom_image').elevateZoom( settings );
        }else{
            if(jQuery("div").is(".zoomContainer")){
                jQuery("div.zoomContainer").remove();
            }
            jQuery('#zoom_image').elevateZoom( settings_size );
        }
        
        //jQuery("#zoom_image").unbind("click");
        jQuery("#zoom_image").bind("click", function(e) {  
            var ez = jQuery('#zoom_image').data('elevateZoom');	
                jQuery.fancybox(ez.getGalleryList());
            return false;
        });
    };
    
    var _resizeTimout = null;
    
    jQuery( window ).resize(function() {
            clearTimeout(_resizeTimout);
           _resizeTimout = setTimeout(function(){
                doc_w = jQuery(document).width();
                _change_doc_w(doc_w);
                _resizeTimout = null;
           },100)
     });
    
    jQuery(document).ready(function() {
        doc_w = jQuery(document).width(); 
        _change_doc_w(doc_w);
        
    });

</script>