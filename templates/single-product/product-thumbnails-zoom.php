<?php
/**
 * Single Product Thumbnails
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product, $woocommerce;

$attachment_ids = $product->get_gallery_attachment_ids();

if ( $attachment_ids ) {
	$loop 		= 0;
	$columns 	= apply_filters( 'woocommerce_product_thumbnails_columns', 3 );
	?>
	<div id="<?php echo get_option('wc_settings_tab_zoom_gallery'); ?>" class="thumbnails <?php echo 'columns-' . $columns; ?>"><?php

		foreach ( $attachment_ids as $attachment_id ) {
                    
                        $classes = array( 'zoom_image_thumbnail' );

			if ( $loop === 0 || $loop % $columns === 0 )
				$classes[] = 'first';

			if ( ( $loop + 1 ) % $columns === 0 )
				$classes[] = 'last';
                        
                        $image_ar = image_downsize($attachment_id, 'shop_magnifier');
                        
                        if($image_ar['3'] == false) {
                           WC_Settings_Tab_Zoom::image_editor_zoom(get_post_thumbnail_id());
                        }
                        
			$image_link = wp_get_attachment_image_url($attachment_id, 'shop_magnifier');
                        $image_link_small = wp_get_attachment_image_url($attachment_id, 'shop_single');

			if ( ! $image_link )
				continue;

			$image_title 	= esc_attr( get_the_title( $attachment_id ) );
			$image_caption 	= esc_attr( get_post_field( 'post_excerpt', $attachment_id ) );

			$image       = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ), 0, $attr = array(
				'title'	=> $image_title,
				'alt'	=> $image_title
				) );

			$image_class = esc_attr( implode( ' ', $classes ) );

			echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<a href="#" class="%s" title="%s" data-image="'.$image_link_small.'" data-zoom-image="'.$image_link.'">%s</a>', $image_class, $image_caption, $image ), $attachment_id, $post->ID, $image_class );

			$loop++;
		}

	?></div>
	<?php
}
