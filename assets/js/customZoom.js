
jQuery( function () {
    
    jQuery("input#wc_settings_tab_zoom_tint").each(function() {
        if ( jQuery( this ).prop('checked') ) {
            
            var value = jQuery('#wc_settings_tab_zoom_zoomType').val();
            
            if(value == 'window'){
                jQuery( '.window_tint_select' ).closest( 'tr' ).show();
                jQuery( '.window_tint_fade' ).closest( 'tr' ).show();
                
                jQuery( '#wc_settings_tab_zoom_lensColour' ).closest( 'tr' ).hide();
                jQuery( '#wc_settings_tab_zoom_lensOpacity' ).closest( 'tr' ).hide();
            }else{
                jQuery( '.window_tint_select' ).closest( 'tr' ).hide();
                jQuery( '.window_tint_fade' ).closest( 'tr' ).hide();
                
                jQuery( '#wc_settings_tab_zoom_lensColour' ).closest( 'tr' ).show();
                jQuery( '#wc_settings_tab_zoom_lensOpacity' ).closest( 'tr' ).show();
            }
        } else {
            jQuery( '.window_tint_select' ).closest( 'tr' ).hide();
            jQuery( '.window_tint_fade' ).closest( 'tr' ).hide();
            
            jQuery( '#wc_settings_tab_zoom_lensColour' ).closest( 'tr' ).show();
            jQuery( '#wc_settings_tab_zoom_lensOpacity' ).closest( 'tr' ).show();
        }
    });
      
    jQuery( '#wc_settings_tab_zoom_zoomType' ).change(function() {
        var value = jQuery(this).val();
        
        if(value === 'window'){
            
            jQuery( '.window-select' ).closest( 'tr' ).show();
            jQuery( '.window_tint_select' ).closest( 'tr' ).show();
            jQuery( '.window_lens_select' ).closest( 'tr' ).show();
            jQuery( '.window_inner_easing' ).closest( 'tr' ).show();
            jQuery( '.window_inner_fade' ).closest( 'tr' ).show();
            jQuery( '.window_lens_fade' ).closest( 'tr' ).show();
            jQuery( '.window_tint_fade' ).closest( 'tr' ).show();
            
            jQuery( '.lens_select' ).closest( 'tr' ).hide();
            jQuery('#wc_settings_tab_zoom_tint').trigger('change')
            
        }else if(value === 'lens'){
            
            jQuery( '.window-select' ).closest( 'tr' ).hide();
            jQuery( '.window_tint_select' ).closest( 'tr' ).hide();
            jQuery( '.window_inner_easing' ).closest( 'tr' ).hide();
            jQuery( '.window_inner_fade' ).closest( 'tr' ).hide();
            jQuery( '.window_tint_fade' ).closest( 'tr' ).hide();
            
            jQuery( '.window_lens_select' ).closest( 'tr' ).show();
            jQuery( '.lens_select' ).closest( 'tr' ).show();
            jQuery( '.window_lens_fade' ).closest( 'tr' ).show();
            
            jQuery( '#wc_settings_tab_zoom_lensColour' ).closest( 'tr' ).show();
            jQuery( '#wc_settings_tab_zoom_lensOpacity' ).closest( 'tr' ).show();
            
        }else if(value === 'inner'){
            
            jQuery( '.window-select' ).closest( 'tr' ).hide();
            jQuery( '.window_tint_select' ).closest( 'tr' ).hide();
            jQuery( '.window_lens_select' ).closest( 'tr' ).hide();
            jQuery( '.lens_select' ).closest( 'tr' ).hide();
            jQuery( '.window_lens_fade' ).closest( 'tr' ).hide();
            jQuery( '.window_tint_fade' ).closest( 'tr' ).hide();
            
            jQuery( '.window_inner_easing' ).closest( 'tr' ).show();
            jQuery( '.window_inner_fade' ).closest( 'tr' ).show();
        }
    }).trigger('change');
    
    jQuery( 'input#wc_settings_tab_zoom_tint' ).change(function() {
        if ( jQuery( this ).is( ':checked' ) ) {
            
            var value = jQuery('#wc_settings_tab_zoom_zoomType').val();
            
            if(value == 'window'){
                console.log('test window');
                jQuery( '.window_tint_select' ).closest( 'tr' ).show();
                jQuery( '.window_tint_fade' ).closest( 'tr' ).show();
                
                jQuery( '#wc_settings_tab_zoom_lensColour' ).closest( 'tr' ).hide();
                jQuery( '#wc_settings_tab_zoom_lensOpacity' ).closest( 'tr' ).hide();
                
            }else{
                jQuery( '.window_tint_select' ).closest( 'tr' ).hide();
                jQuery( '.window_tint_fade' ).closest( 'tr' ).hide();
                
                jQuery( '#wc_settings_tab_zoom_lensColour' ).closest( 'tr' ).show();
                jQuery( '#wc_settings_tab_zoom_lensOpacity' ).closest( 'tr' ).show();
            }
        } else {
            jQuery( '.window_tint_select' ).closest( 'tr' ).hide();
            jQuery( '.window_tint_fade' ).closest( 'tr' ).hide();
            
            jQuery( '#wc_settings_tab_zoom_lensColour' ).closest( 'tr' ).show();
            jQuery( '#wc_settings_tab_zoom_lensOpacity' ).closest( 'tr' ).show();
        }
    }).change();
    
    jQuery( 'input#wc_settings_tab_zoomEnabled' ).change(function() {
          var _this = jQuery(this);
          var table = jQuery(this).closest('table');
          if ( _this.is( ':checked' ) ) {
              table.find('tr').not(':first').show();
              jQuery( '#wc_settings_tab_zoom_zoomType, input#wc_settings_tab_zoom_tint' ).trigger('change');
          }else{
              table.find('tr').not(':first').hide();
          }
    }).trigger('change');
    
});

